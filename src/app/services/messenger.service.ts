import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MessengerService {
  constructor() {}

  subject = new Subject();

  // tslint:disable-next-line: typedef
  sendMessage(product) {
    this.subject.next(product); // triggering an event //subject is used to send data
  }
  // tslint:disable-next-line: typedef
  getMessage() {
    return this.subject.asObservable(); // observable is used to keep track of data
  }
}
