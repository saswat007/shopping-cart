import { environment } from './../../environments/environment';
export const baseUrl = environment.production
  ? 'http://shoppingcart.com'
  : 'http://localhost:3000';
export const productsUrl = baseUrl + '/products/';
export const cartUrl = baseUrl + '/cart/';
export const wishListUrl = baseUrl + '/wishlist';
export const userUrl = baseUrl + '/users';
