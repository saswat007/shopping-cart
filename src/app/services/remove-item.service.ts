import { cartUrl } from './../config/api';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class RemoveItemService {

  private newList = new Subject<string>();
  $itemList = this.newList.asObservable();


  constructor(private http: HttpClient) {}

  // tslint:disable-next-line: typedef
  removeFromCart(item): Observable<any> {
    // console.log(item);
    return this.http.get<any>(cartUrl).pipe(
      map((result: any[]) => {
        result.forEach((itemapi) => {
          if (item.id === itemapi.product.id) {
            // tslint:disable-next-line: prefer-const
            let endpoint = itemapi.id;
            this.http.delete(cartUrl + endpoint).subscribe(() => {
             // console.log('done');
              this.newList.next('update req sent');
            });
           // console.log('end reached');
          }
        });
      })
    );
  }
}
