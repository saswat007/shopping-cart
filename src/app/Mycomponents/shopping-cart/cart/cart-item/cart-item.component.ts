import { RemoveItemService } from './../../../../services/remove-item.service';
import { Component, Input, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.css'],
})
export class CartItemComponent implements OnInit {
  @Input() initem;
  constructor(
    private removeserv: RemoveItemService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {}

  // tslint:disable-next-line: typedef
  remove() {
    this.spinnerRun();
    this.removeserv.removeFromCart(this.initem).subscribe((item) => {
      // console.log(item);
    });
  }
  // tslint:disable-next-line: typedef
  spinnerRun() {
  //  console.log('spinner started');
    this.spinner.show(undefined, {
      type : 'ball-spin-clockwise'
    });
    setTimeout(() => {
      this.spinner.hide();
    }, 2000);
  // console.log('spinner stopped');
  }
}
