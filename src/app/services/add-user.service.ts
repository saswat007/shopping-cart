import { userUrl } from './../config/api';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable, observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AddUserService {
  constructor(private http: HttpClient) {}

  // tslint:disable-next-line: typedef
  addUser(users) {
    return this.http.post(userUrl, { users });
  }

  // tslint:disable-next-line: typedef
  getPreviousUsers() {
    return this.http.get(userUrl).pipe(
      map((result: any[]) => {
        // tslint:disable-next-line: prefer-const
        let prevusers = [];
        result.forEach((item) => {
          prevusers.push(item.users.username);
        });
        return prevusers;
      })
    );
  }
}
