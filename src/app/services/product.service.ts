import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../model/product';
import { productsUrl } from 'src/app/config/api';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  product: Product[] = [];
  minValue = 100;
  maxValue = 1000;

  // to be used for non api data
  // product: Product[] = [
  //   new Product(
  //     1,
  //     'stuffed animal',
  //     'made from recycled materials',
  //     'https://assets.ajio.com/medias/sys_master/root/hcf/h3d/14233206620190/-473Wx593H-4912802430-multi-MODEL.jpg',
  //     500
  //   ),
  //   new Product(
  //     2,
  //     'rc toy',
  //     'made from recycled materials',
  //     'https://assets.ajio.com/medias/sys_master/root/ajio/catalog/5ef38d9f7cdb8c721b726816/-473Wx593H-4916029130-multi-MODEL.jpg',
  //     700
  //   ),
  //   new Product(
  //     3,
  //     'toy car',
  //     'made from recycled materials',
  //     'https://assets.ajio.com/medias/sys_master/root/h52/h92/15605125283870/-473Wx593H-4913741070-multi-MODEL.jpg',
  //     800
  //   ),
  //   new Product(
  //     4,
  //     'toy boat',
  //     'made from recycled materials',
  //     'https://assets.ajio.com/medias/sys_master/root/h46/hac/12539980152862/-473Wx593H-4906580920-multi-MODEL2.jpg',
  //     900
  //   ),
  //   new Product(
  //     5,
  //     'toy bus',
  //     'made from recycled materials',
  //     'https://assets.ajio.com/medias/sys_master/root/hb2/hdf/13912745345054/-473Wx593H-460405673-multicolour-MODEL.jpg',
  //     1000
  //   ),
  //   new Product(
  //     6,
  //     'toy train',
  //     'made from recycled materials',
  //     'https://assets.ajio.com/medias/sys_master/root/hee/h47/12952124981278/-473Wx593H-4913740870-multi-MODEL.jpg',
  //     1500
  //   ),
  // ];

  constructor(private http: HttpClient) {}

  //   // below is used in order to get the data from the product array[] where product model is used
  // getProducts(): Product[] {
  //   // can be populated using apis and return an observable
  //   return this.product;
  // }

  // used for receiving updated value from filtercomponent.ts

  // filterProducts(minprice , maxprice){

  //   console.log("inside filterProducts method in service");
  //   this.minValue = minprice;
  //   this.maxValue = maxprice;
  //   //this.getProducts();

  // }

  getProducts(minvalue , maxvalue): Observable<Product[]> {
    return this.http.get<Product[]>(productsUrl).pipe(
      map((result: any[]) => {
        // tslint:disable-next-line: prefer-const
        let pipeproducts = [];
        result.forEach((item) => {
          if (item.price >= minvalue && item.price <= maxvalue) {
            pipeproducts.push(item);
          }
        });
        return pipeproducts;
      })
    );
  }
}
