import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class NotifyLoginService {
  // tslint:disable-next-line: variable-name
  private _regsource = new Subject<string>();
  $regUser = this._regsource.asObservable();

  constructor() {}

  // tslint:disable-next-line: typedef
  regNotification(msg: string) {
    this._regsource.next(msg);
  }
}
