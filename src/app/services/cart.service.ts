import { Product } from './../model/product';
import { cartUrl } from './../config/api';
import { HttpClient } from '@angular/common/http';
import { Cartitem } from './../model/cart-item';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  constructor(private http: HttpClient) {}

  getCartItems(): Observable<Cartitem[]> {
    return this.http.get<Cartitem[]>(cartUrl).pipe(
      map((result: any[]) => {
        // tslint:disable-next-line: prefer-const
        let cartitems: Cartitem[] = [];

        // tslint:disable-next-line: prefer-const
        for (let item of result) {
          let productExist = false;

          cartitems.forEach((items) => {
            if (items.id === item.product.id) {
              items.qty++;
              productExist = true;
            }
          });

          if (!productExist) {
            cartitems.push(new Cartitem(item.product));
          }
        }

        return cartitems;
      })
    );
  }

  // tslint:disable-next-line: typedef
  addToCart(product: Product): Observable<any> {
    return this.http.post<any>(cartUrl, { product });
  }
}
