import { RemoveItemService } from './../../../services/remove-item.service';
import { Cartitem } from './../../../model/cart-item';
import { CartService } from './../../../services/cart.service';
import { MessengerService } from './../../../services/messenger.service';
import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/model/product';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
})
export class CartComponent implements OnInit {
  cartitem = [];
  carttotal = 0;

  constructor(
    private msg: MessengerService,
    private cart: CartService,
    private remove: RemoveItemService
  ) {}

  ngOnInit(): void {
    this.handleSubscription();
    this.loadCartItems();
    this.remove.$itemList.subscribe((msg) => {
      // console.log('oninit fired');
      this.loadCartItems();
      // console.log(msg);
    });
  }

  // tslint:disable-next-line: typedef
  handleSubscription() {
    this.msg.getMessage().subscribe((product: Product) => {
      this.loadCartItems();
    });
  }

  // tslint:disable-next-line: typedef
  loadCartItems() {
    this.cart.getCartItems().subscribe((item: Cartitem[]) => {
      this.cartitem = item;
      this.calCartTotal();
    });
  }

  // tslint:disable-next-line: typedef
  // toCart(product: Product) {
  //   let productExist = false;

  //   this.cartitem.forEach((item) => {
  //     console.log(item);
  //     if (item.id === product.id) {
  //       item.qty++;
  //       productExist = true;
  //     }
  //   });

  //   if (!productExist) {
  //     this.cartitem.push({
  //       id: product.id,
  //       title: product.title,
  //       price: product.price,
  //       qty: 1,
  //     });
  //   }

  // logic 1 which caused issues
  // console.log(product.id);
  // // tslint:disable-next-line: prefer-const
  // if (this.cartitem.length > 0) {
  //   for (let i in this.cartitem) {
  //     if (this.cartitem[i].id === product.id) {
  //       // this.cartitem[i].qty++;
  //       this.cartitem.push({
  //         id: product.id,
  //         title: product.title,
  //         price: product.price,
  //         qty: this.cartitem[i].qty++,
  //       });
  //       this.cartitem.splice(this.cartitem[i], 1);
  //     } else {
  //       this.cartitem.push({
  //         id: product.id,
  //         title: product.title,
  //         price: product.price,
  //         qty: 1,
  //       });
  //     }
  //   }

  // logic 2 which caused issue to be used inside if
  // this.cartitem.forEach((item) => {
  //   console.log(item);
  //   if (item.id !== product.id) {
  //     this.cartitem.push({
  //       id: product.id,
  //       title: product.title,
  //       price: product.price,
  //       qty: 1,
  //     });
  //   } else {
  //     console.log('inside foreach if');
  //     this.cartitem.push({
  //       id: product.id,
  //       title: product.title,
  //       price: product.price,
  //       qty: item.qty++,
  //     });
  //     console.log(`quntity added +1 = ${item.qty}`);
  //   }
  // });

  // else same for both logic
  // } else {
  //   this.cartitem.push({
  //     id: product.id,
  //     title: product.title,
  //     price: product.price,
  //     qty: 1,
  //   });
  // }
  //   this.calCartTotal();
  // }
  // tslint:disable-next-line: typedef
  calCartTotal() {
    this.carttotal = 0;
    this.cartitem.forEach((item) => {
      this.carttotal += item.qty * item.price;
    });
  }
}
