import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class InteractionService {

  // tslint:disable-next-line: variable-name
  private _loginSource = new Subject<string>();
  $loginUser = this._loginSource.asObservable();

  constructor() {}

  // tslint:disable-next-line: typedef
  loginData(username: string){
    this._loginSource.next(username);
  }
}
