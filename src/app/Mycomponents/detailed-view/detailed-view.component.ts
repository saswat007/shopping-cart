import { NgxSpinnerService } from 'ngx-spinner';
import { MessengerService } from './../../services/messenger.service';
import { WishlistService } from './../../services/wishlist.service';
import { CartService } from './../../services/cart.service';
import { GetSingleProductService } from './../../services/get-single-product.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detailed-view',
  templateUrl: './detailed-view.component.html',
  styleUrls: ['./detailed-view.component.css'],
})
export class DetailedViewComponent implements OnInit {
  id: number = null;
  productitem: any = [];
  addedToWishList = false;
  constructor(
    private route: ActivatedRoute,
    private single: GetSingleProductService,
    private cart: CartService,
    private wish: WishlistService,
    private msg: MessengerService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.showSpinner();
    this.getRouteParam();
    this.getSingleProduct();
  }
  // tslint:disable-next-line: typedef
  getRouteParam() {
    const routeparam = this.route.snapshot.paramMap.get('id');
    this.id = +routeparam;
    // console.log(this.id);
  }
  // tslint:disable-next-line: typedef
  getSingleProduct() {
    this.single.getOneProduct(this.id).subscribe((oneproduct) => {
      this.productitem = oneproduct;
      // console.log(this.productone);
    });
  }
  // tslint:disable-next-line: typedef
  addToCart() {
    this.cart.addToCart(this.productitem).subscribe(() => {
      this.msg.sendMessage(this.productitem);
    });
  }
  // tslint:disable-next-line: typedef
  handleAddToWishList() {
    this.wish.addToWishList(this.productitem.id).subscribe(() => {
      this.addedToWishList = true;
    });
  }
  // tslint:disable-next-line: typedef
  handleRemoveFromWishList() {
    this.wish.removeFromWishList(this.productitem.id).subscribe(() => {
      this.addedToWishList = false;
    });
  }
  // tslint:disable-next-line: typedef
  showSpinner() {
    //console.log('spinner started');
    this.spinner.show(undefined,{
      type : 'line-scale'
    });
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
   // console.log('spinner stopped');
  }
}
