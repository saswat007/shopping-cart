import { TestBed } from '@angular/core/testing';

import { MatchUserService } from './match-user.service';

describe('MatchUserService', () => {
  let service: MatchUserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MatchUserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
