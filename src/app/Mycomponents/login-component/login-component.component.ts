import { NotifyLoginService } from './../../services/notify-login.service';
import { InteractionService } from './../../services/interaction.service';
import { MatchUserService } from './../../services/match-user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.css'],
})
export class LoginComponentComponent implements OnInit {
  customModel: any = {};
  result = [];
  loggedinUser = '';
  switch = false;

  constructor(
    private match: MatchUserService,
    // tslint:disable-next-line: variable-name
    private _interaction: InteractionService,
    private router: Router,
    private notify: NotifyLoginService
  ) {}

  ngOnInit(): void {
    // this.getNotification();
    // console.log('inside oninit');
    // console.log('initial msg length');
    // console.log(this.loggedinUser.length);
    this.notify.$regUser.subscribe((msg) => {
      this.loggedinUser = msg;
     //  console.log('after getting msg length');
     //  console.log(this.loggedinUser.length);
    });
  }

  // tslint:disable-next-line: typedef
  login() {
    this.match
      .matchUser(this.customModel.username, this.customModel.password)
      .subscribe((user) => {
        if (user.length !== 0) {
          // console.log(user);
          this.loggedinUser = user.toString();
          // console.log(this.loggedinUser);
          this.switch = false;
          this.sendUser(this.loggedinUser);
          this.router.navigate(['/shop']);
        } else {
          this.switch = true;
          // console.log(this.switch);
        }
      });
  }
  // tslint:disable-next-line: typedef
  sendUser(username) {
    this._interaction.loginData(username);
  }
  // tslint:disable-next-line: typedef
  // getNotification() {
  //   console.log('get notification working');
  //   console.log('initial length of loggedinuser');
  //   console.log(this.loggedinUser.length);

  // }
}
