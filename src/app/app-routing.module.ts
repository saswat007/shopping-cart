import { DetailedViewComponent } from './Mycomponents/detailed-view/detailed-view.component';
import { ContactComponent } from './Mycomponents/contact/contact.component';
import { AboutComponent } from './Mycomponents/about/about.component';
import { ErrorPageComponent } from './Mycomponents/error-page/error-page.component';
import { ShoppingCartComponent } from './Mycomponents/shopping-cart/shopping-cart.component';
import { RegisterComponentComponent } from './Mycomponents/register-component/register-component.component';
import { LoginComponentComponent } from './Mycomponents/login-component/login-component.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path : '' , redirectTo : 'shop' , pathMatch : 'full'},
  { path : 'login' , component : LoginComponentComponent },
  { path : 'register' , component : RegisterComponentComponent},
  { path : 'shop' , component : ShoppingCartComponent},
  { path : 'about' , component : AboutComponent},
  { path : 'contact' , component : ContactComponent},
  { path : 'view/:id' , component : DetailedViewComponent},
  { path : '**' , component : ErrorPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
