import { NgxSpinnerService } from 'ngx-spinner';
import { InteractionService } from './../../../services/interaction.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css'],
})
export class NavBarComponent implements OnInit {
  user = '';

  // tslint:disable-next-line: variable-name
  constructor(private _interaction: InteractionService, private spinner : NgxSpinnerService) {}

  ngOnInit(): void {
    this._interaction.$loginUser.subscribe((username) => {
      this.user = username;
    });
  }
  // tslint:disable-next-line: typedef
  logout() {
    this.user = '';
  }
  spinnerAnimate(){
    this.spinner.show(undefined,{type : 'timer'});
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);

  }
}
