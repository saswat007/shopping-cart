import { NotifyLoginService } from './../../services/notify-login.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AddUserService } from 'src/app/services/add-user.service';

@Component({
  selector: 'app-register-component',
  templateUrl: './register-component.component.html',
  styleUrls: ['./register-component.component.css'],
})
export class RegisterComponentComponent implements OnInit {
  registermodel: any = { name: '', username: '', email: '', password: '' };
  switch = false;
  prevUsers = [];
  tempnum = 0;
  notification = '';

  constructor(
    private user: AddUserService,
    private router: Router,
    private notify: NotifyLoginService
  ) {}

  ngOnInit(): void {
    this.getUsers();
  }

  // tslint:disable-next-line: typedef
  register() {
    // console.log('inside register');
    if (this.prevUsers.length > 0) {
      // console.log('inside main if');
      let productExist = false;
      this.prevUsers.forEach((item) => {
        if (item === this.registermodel.username) {
          productExist = true;
          this.switch = true;
          //  console.log('user exist');
          // console.log(productExist);
        }
      });
      if (!productExist) {
        this.router.navigate(['/login']);
        this.user.addUser(this.registermodel).subscribe(() => {
          this.notification = 'success !';

          this.sendNotification(this.notification);
          //  console.log('username added through 2nd if block');
          // this.router.navigate(['/login']);
        });
      }
    } else {
      this.router.navigate(['/login']);
      this.user.addUser(this.registermodel).subscribe(() => {
        this.notification = 'success !';

        this.sendNotification(this.notification);
        //  console.log('username added through main else block');
        // this.router.navigate(['/login']);
      });
    }
  }

  // tslint:disable-next-line: typedef
  // register() {
  //   console.log('inside register()');
  //   if (this.prevUsers.length > 0) {
  //     console.log('inside if');
  //     this.prevUsers.forEach((item) => {
  //       if (item !== this.registermodel.username) {
  //         console.log('username doesnot exist');
  //         this.user.addUser(this.registermodel).subscribe(() => {
  //           this.switch = true;
  //           console.log('username added');
  //           this.router.navigate(['/login']);
  //         });
  //       } else {
  //         this.flag = false;
  //         console.log('username exist');
  //       }
  //     });
  //   }
  //   if (this.flag) {
  //     console.log('inside else');
  //     this.user.addUser(this.registermodel).subscribe(() => {
  //       this.switch = true;
  //       this.router.navigate(['/shop']);
  //     });
  //   }
  // }

  // tslint:disable-next-line: typedef
  getUsers() {
    this.user.getPreviousUsers().subscribe((prev) => {
      this.prevUsers = prev;
      //  console.log('from prevusers array');
      // console.log(this.prevUsers);
    });
  }
  // tslint:disable-next-line: typedef
  sendNotification(note) {
    this.notify.regNotification(note);
  }
}
