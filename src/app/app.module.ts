import { MaterialModule } from './material/material.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './Mycomponents/shared/nav-bar/nav-bar.component';
import { HeaderComponent } from './Mycomponents/shared/header/header.component';
import { FooterComponent } from './Mycomponents/shared/footer/footer.component';
import { ShoppingCartComponent } from './Mycomponents/shopping-cart/shopping-cart.component';
import { FiltersComponent } from './Mycomponents/shopping-cart/filters/filters.component';
import { ProductListComponent } from './Mycomponents/shopping-cart/product-list/product-list.component';
import { CartComponent } from './Mycomponents/shopping-cart/cart/cart.component';
import { CartItemComponent } from './Mycomponents/shopping-cart/cart/cart-item/cart-item.component';
import { ProductItemComponent } from './Mycomponents/shopping-cart/product-list/product-item/product-item.component';
import { LoginComponentComponent } from './Mycomponents/login-component/login-component.component';
import { RegisterComponentComponent } from './Mycomponents/register-component/register-component.component';
import { ErrorPageComponent } from './Mycomponents/error-page/error-page.component';
import { AboutComponent } from './Mycomponents/about/about.component';
import { ContactComponent } from './Mycomponents/contact/contact.component';
import { FormsModule } from '@angular/forms';
import { EqualvalidatorDirective } from './customValidator/equalvalidator.directive';
import { DetailedViewComponent } from './Mycomponents/detailed-view/detailed-view.component';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    HeaderComponent,
    FooterComponent,
    ShoppingCartComponent,
    FiltersComponent,
    ProductListComponent,
    CartComponent,
    CartItemComponent,
    ProductItemComponent,
    LoginComponentComponent,
    RegisterComponentComponent,
    ErrorPageComponent,
    AboutComponent,
    ContactComponent,
    EqualvalidatorDirective,
    DetailedViewComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
   MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}
