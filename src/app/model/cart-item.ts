import { Product } from './product';
export class Cartitem {
  id: number;
  title: string;
  price: number;
  qty: number;

  constructor(product: Product, qty = 1) {
    this.id = product.id;
    this.title = product.title;
    this.price = product.price;
    this.qty = qty;
  }
}
