import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css'],
})
export class ShoppingCartComponent implements OnInit {
  minprice = 100;
  maxprice = 1000;

  constructor() {}

  // tslint:disable-next-line: typedef
  filterData(event) {
    this.minprice = event.minprice;
    this.maxprice = event.maxprice;
  }

  ngOnInit(): void {}
}
