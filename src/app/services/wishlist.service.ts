import { HttpClient } from '@angular/common/http';
import { wishListUrl } from './../config/api';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class WishlistService {
  constructor(private http: HttpClient) {}

  // tslint:disable-next-line: typedef
  getFromWishList() {
    return this.http.get(wishListUrl).pipe(
      map((result: any[]) => {
        // tslint:disable-next-line: prefer-const
        let productids = [];
        result.forEach((item) => productids.push(item.id));
        return productids;
      })
    );
  }
  // tslint:disable-next-line: typedef
  addToWishList(productid) {
    return this.http.post(wishListUrl, { id: productid });
  }
  // tslint:disable-next-line: typedef
  removeFromWishList(productid) {
    return this.http.delete(wishListUrl + '/' + productid);
  }
}
