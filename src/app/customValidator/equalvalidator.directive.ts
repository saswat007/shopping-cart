import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
} from '@angular/forms';
import { Directive, Input } from '@angular/core';

@Directive({
  selector: '[appEqualvalidator]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: EqualvalidatorDirective,
      multi: true,
    },
  ],
})
export class EqualvalidatorDirective implements Validator {
  constructor() {}
  @Input() appEqualvalidator: string;
  validate(control: AbstractControl): { [key: string]: any } | null {
    const controlToCompare = control.parent.get(this.appEqualvalidator);
    if (controlToCompare && controlToCompare.value !== control.value) {
      return { notEqual: true };
    }
    return null;
  }
}
