import { Product } from './../model/product';
import { Observable } from 'rxjs';
import { productsUrl } from './../config/api';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class GetSingleProductService {
  constructor(private http: HttpClient) {}

  // tslint:disable-next-line: typedef
  getOneProduct(id) {
    const endpoint = id;
    return this.http.get(productsUrl + endpoint);
  }
}
