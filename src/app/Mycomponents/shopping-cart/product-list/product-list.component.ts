import { WishlistService } from './../../../services/wishlist.service';
import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { Product } from 'src/app/model/product';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
})
export class ProductListComponent implements OnInit, OnChanges {
  productList: Product[];
  wishList: number[] = [];
  @Input() minprice: number;
  @Input() maxprice: number;

  constructor(
    private productService: ProductService,
    private wishlist: WishlistService
  ) {}
  // below will be used incase of local data not apis

  // ngOnInit(): void {
  //   this.productList = this.productService.getProducts();
  //   console.log(this.productList);
  // }
  ngOnInit(): void {
    // console.log(this.minprice, this.maxprice);
    this.loadProducts(this.minprice, this.maxprice);
    this.loadWishList();
  }
  // tslint:disable-next-line: typedef
  loadProducts(minvalue, maxvalue) {
    // console.log('from load products method ' + minvalue, maxvalue);
    this.productService
      .getProducts(minvalue, maxvalue)
      .subscribe((products) => {
        this.productList = products;
        // console.log(this.productList);
      });
  }
  // tslint:disable-next-line: typedef
  loadWishList() {
    this.wishlist.getFromWishList().subscribe((productids) => {
      // console.log(productids);
      this.wishList = productids;
    });
  }
  // tslint:disable-next-line: typedef
  ngOnChanges(changes: SimpleChanges) {
    // console.log(changes);
    this.loadProducts(this.minprice, this.maxprice);
    // const minvalue = changes['minprice'];
    // const maxvalue = changes['maxprice'];
    // if(minvalue.firstChange === false || maxvalue.firstChange === false){
    //   this.loadProducts(this.minprice , this.maxprice);
    // }
  }
}
