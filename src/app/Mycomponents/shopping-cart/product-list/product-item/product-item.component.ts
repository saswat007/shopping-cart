import { CartService } from './../../../../services/cart.service';
import { WishlistService } from './../../../../services/wishlist.service';
import { MessengerService } from './../../../../services/messenger.service';
import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/model/product';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css'],
})
export class ProductItemComponent implements OnInit {
  @Input() productitem: Product;
  @Input() addedToWishList: boolean;

  constructor(
    private msg: MessengerService,
    private wish: WishlistService,
    private cart: CartService
  ) {}

  ngOnInit(): void {}

  // tslint:disable-next-line: typedef
  addToCart() {
    this.cart.addToCart(this.productitem).subscribe(() => {
      this.msg.sendMessage(this.productitem);
    });
  }
  // tslint:disable-next-line: typedef
  handleAddToWishList() {
    this.wish.addToWishList(this.productitem.id).subscribe(() => {
      this.addedToWishList = true;
    });
  }
  // tslint:disable-next-line: typedef
  handleRemoveFromWishList() {
    this.wish.removeFromWishList(this.productitem.id).subscribe(() => {
      this.addedToWishList = false;
    });
  }
}
