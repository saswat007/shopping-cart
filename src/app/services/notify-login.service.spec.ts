import { TestBed } from '@angular/core/testing';

import { NotifyLoginService } from './notify-login.service';

describe('NotifyLoginService', () => {
  let service: NotifyLoginService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NotifyLoginService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
