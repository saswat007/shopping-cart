import { NgxSpinnerService } from 'ngx-spinner';
import { ProductService } from 'src/app/services/product.service';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.css'],
})
export class FiltersComponent implements OnInit {
  prices = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000];
  prices1 = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000];

  constructor(private service: ProductService, private spinner: NgxSpinnerService) {}

  @Output() emitter = new EventEmitter();

  ngOnInit(): void {}

  // tslint:disable-next-line: typedef
  filter(minprice, maxprice) {
    this.spinner.show(undefined,{ type : 'ball-pulse'})
    this.emitter.emit({ minprice, maxprice });
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
  }
}
