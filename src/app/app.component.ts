import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Angular-Shoppingcart';
  constructor(private spinner : NgxSpinnerService) {}

  ngOnInit(){
    this.spinner.show(undefined,{type : 'ball-fussion',bdColor : 'rgba(51,51,100,1.0)'});
    setTimeout(() => {
      this.spinner.hide();
    },3000);
  }
}
