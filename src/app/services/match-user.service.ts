import { HttpClient } from '@angular/common/http';
import { userUrl } from './../config/api';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class MatchUserService {
  constructor(private http: HttpClient) {}

  matchUser(username, password): Observable<any[]> {
    return this.http.get<any[]>(userUrl).pipe(
      map((result: any[]) => {
        const pipeusers = [];
        result.forEach((item) => {
          if (
            username === item.users.username &&
            password === item.users.password
          ) {
            pipeusers.push(item.users.name);
          }
        });
        return pipeusers;
      })
    );
  }
}
